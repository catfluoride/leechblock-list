# Leechblock list

This repository is just a collection of web addresses to be used in [LeechBlock NG extension](https://addons.mozilla.org/en-US/firefox/addon/leechblock-ng/) for [Firefox 57+](https://firefox.com). Get rid of all these tempting and distracting websites and boost your productivity!